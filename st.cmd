require adhama

epicsEnvSet("PREFIX", "Hama:")
epicsEnvSet("PORT",   "HAMA")
epicsEnvSet("CAMERA", "0")
epicsEnvSet("QSIZE",  "21")
epicsEnvSet("XSIZE",  "10")
epicsEnvSet("YSIZE",  "10")
epicsEnvSet("NCHANS", "2048")
# Number of Elements
epicsEnvSet("NELEMENTS", "4194304")
epicsEnvSet("EPICS_CA_MAX_ARRAY_BYTES", "64000000")

# iocshLoad("$(adhama_DIR)ADHama.iocsh")

devHamamatsuConfig("$(PORT)", $(CAMERA), 0, 0, 0, 0, 10)

dbLoadRecords("$(adhama_DIR)db/hama.db","P=$(PREFIX),R=cam1:,EVR=LabS-Utgard-VIP:TS-EVR-3,E=F,PORT=$(PORT),ADDR=0,TIMEOUT=1")

#asynSetTraceMask("$(PORT)",-1,0x9) 
#asynSetTraceIOMask("$(PORT)",-1,0x2)

# =========================================================================================================
# Create a PVA arrays plugin
# NDPvaConfigure (const char *portName, int queueSize, int blockingCallbacks,
#                      const char *NDArrayPort, int NDArrayAddr, const char *pvName,
#                      size_t maxMemory, int priority, int stackSize)
NDPvaConfigure("PVA1", $(QSIZE), 0, "$(PORT)", 0, "$(PREFIX)Pva1:Image", 0, 0, 0)
dbLoadRecords("$(adcore_DIR)/db/NDPva.template",  "P=$(PREFIX),R=Pva1:, PORT=PVA1,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT)")
#

# Create a standard arrays plugin, set it to get data from hama driver.
NDStdArraysConfigure("Image1", "$(QSIZE)", 0, "$(PORT)", 0, 0)
dbLoadRecords("NDStdArrays.template", "P=$(PREFIX),R=image1:,PORT=Image1,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),TYPE=Int32,FTVL=LONG,NELEMENTS=$(NELEMENTS)")

startPVAServer
iocInit()

dbpf Hama:cam1:PoolUsedMem.SCAN 0
dbpf Hama:image1:EnableCallbacks 1

epicsThreadSleep(1.0)
